#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>


struct SquareTable {
    int n;
    int width;
    int *rawTable;

    SquareTable(int n) : n(n), width(n + 1) {
        rawTable = (int *) calloc(width * width, sizeof(int));
    }

    ~SquareTable() {
        free(rawTable);
    }

    inline void Set(int x, int y, int value) {
        std::cout << "(" << x << ", " << y << ") = " << value << std::endl;
        rawTable[width * x + y] = value;
    }

    void DisableAllOtherPathes(int point) {
        for (int i = 1; i <= n; ++i) {
            if (0 == Get(point, i)) {
                Set(point, i, -1);
            }
        }
    }

    inline int Get(int x, int y) {
        return rawTable[width * x + y];
    }

    void PrintTable() {
        std::stringstream ss;
        for (int x = 1; x <= n; ++x) {
            for (int y = 1; y <= n; ++y) {
                ss << Get(x, y) << ' ';
            }
            ss << std::endl;
        }
        std::cout << ss.str();
    }
};

struct BestPath {
    int n;
    int currentIndex;
    int *path;
    bool *usedPoints;


    BestPath(int n) : n(n), currentIndex(0) {
        path = (int *) calloc(n, sizeof(int));
        usedPoints = (bool *) calloc(n + 1, sizeof(bool));
    }

    ~BestPath() {
        free(path);
        free(usedPoints);
    }

    void Clear() {
        currentIndex = 0;
        memset(usedPoints, 0, (n + 1) * sizeof(bool));
    }

    bool Contains(int point) {
        return usedPoints[point];
    }

    bool AddPoint(SquareTable &processingTable, int y) {
        if (!Contains(y)) {
            path[currentIndex] = y;
            for (int i = currentIndex; i > 0; --i) {
                processingTable.Set(path[i - 1], y, path[i]);
            }
            currentIndex++;
            usedPoints[y] = 1;
            return true;
        } else {
            processingTable.Set(path[currentIndex - 1], y, y);
            for (int i = currentIndex - 1; i > 0; --i) {
                processingTable.DisableAllOtherPathes(path[i]);
            }
            return false;
        }
    }
};

void FindBestPath(SquareTable &initialTable, SquareTable &processingTable, BestPath &path, int n, int x) {
    for (int y = 1; y <= n; ++y) {
        if (0x00 == processingTable.Get(x, y)) {
            if (initialTable.Get(x, y)) {
                if (!path.AddPoint(processingTable, y)) {
                    break;
                } else {
                    FindBestPath(initialTable, processingTable, path, n, y);
                }
            }
        }
    }

}

int main() {
    std::ifstream in("test");
    std::cin.rdbuf(in.rdbuf());

    int n, m, q;
    int x, y;
    std::cin >> n >> m >> q;
    SquareTable initialTable(n);
    BestPath path(n);

    for (int i = 0; i < m; ++i) {
        std::cin >> x >> y;
        initialTable.Set(x, y, 1);
    }
    SquareTable processedTable(n);

    for (int x = 1; x <= n; ++x) {
        std::cout << "X = " << x << std::endl;
        path.Clear();
        path.AddPoint(processedTable, x);
        FindBestPath(initialTable, processedTable, path, n, x);
        processedTable.DisableAllOtherPathes(x);
        processedTable.PrintTable();

    }


    return 0;
}