#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <list>
#include <set>
#include <vector>
#include <algorithm>
#include <iomanip>

#define NO_PATH -1
#define LOOPBACK -2
#define PATH_EXIST 0

struct SquareTable {
    int n;
    int width;
    // Contains path from specific vertex (as raw) to the other (as column)
    int *rawTable;

    void BFS(int root, std::vector<std::list<int>> &pathsFrom, std::set<int> &visited) {
        std::list<int> toVisit;
        int next = root;
        while (true) {
            for (auto i : pathsFrom[next]) {
                if (0 == visited.count(i)) {
                    toVisit.push_back(i);
                    Set(i, root, PATH_EXIST);
                }
            }
            visited.insert(next);
            if (toVisit.size() > 0) {
                next = toVisit.front();
                toVisit.pop_front();
            } else {
                break;
            }
        }
        visited.erase(root);
    }


    void FindBestPath(int root, std::vector<std::list<int>> &pathsTo, std::set<int> &vertexesHavePath) {
        int temp, prev;
        int next = -1;
        std::list<int> bestPath;
        while (true) {
            if (next == -1) {
                if (vertexesHavePath.empty()) {
                    break;
                } else {
                    auto it = vertexesHavePath.begin();
                    next = *it;
                    vertexesHavePath.erase(it);
                }
            }

            bestPath.push_back(next);
            int neighbour;
            for (neighbour : pathsTo[next]) {
                if (root == neighbour) {
                    Set(next, root, root);
                    prev = next;
                    for (auto item : bestPath) {
                        Set(item, root, prev);
                        prev = item;
                    }
                    next = -1;
                    bestPath.clear();
                } else {
                    temp = Get(neighbour, root);
                    if (NO_PATH == temp)
                        continue;
                    else if (PATH_EXIST == temp) {
                        next = temp;
                        vertexesHavePath.erase(next);
                        break;
                    } else if (LOOPBACK == temp) {
                        for (auto item : bestPath)
                            Set(item, root, LOOPBACK);
                        next = -1;
                        bestPath.clear();
                    } else {
                        if (std::find(bestPath.begin(), bestPath.end(), temp) != bestPath.end()) {
                            Set(temp, root, LOOPBACK);
                            for (auto item : bestPath)
                                Set(item, root, LOOPBACK);
                            next = -1;
                            bestPath.clear();
                        } else {
                            next = temp;
                        }
                    }
                }
            }
        }
    }

    SquareTable(int n, std::vector<std::list<int>> &pathsFrom, std::vector<std::list<int>> &pathsTo) : n(n),
                                                                                                       width(n + 1) {
        rawTable = (int *) malloc(width * width * sizeof(int));
        memset(rawTable, NO_PATH, width * width * sizeof(int));
        std::set<int> visited;
        for (int i = 1; i < width; ++i) {
            std::cout << "\nRoot: " << i << std::endl;
//            BFS(i, pathsFrom);
            BFS(i, pathsFrom, visited);
            std::cout << "Vertex from (" << visited.size() << "): ";
            for (auto v : visited)
                std::cout << v << " ";
            FindBestPath(i, pathsTo, visited);
            visited.clear();
            PrintTable();
        }
        PrintTable();
    }

    int GetKFromPath(int start, int finish, int k) {
        int next = Get(start, finish);
        if (next >= 0) {
            if (k == 1) {
                next = start;
            } else {
                for (int i = 1; i < k; ++i) {
                    next = Get(start, finish);
                }
            }
        } else {
            next = -1;
        }

        return next;
    }

    ~SquareTable() {
        free(rawTable);
    }

    inline void Set(int x, int y, int value) {
//        std::cout << "(" << x << ", " << y << ") = " << value << std::endl;
        rawTable[width * x + y] = value;
    }

    inline int Get(int x, int y) {
        return rawTable[width * x + y];
    }
//
//    void DisableAllOtherpathsFrom(int point) {
//        for (int i = 1; i <= n; ++i) {
//            if (0 == Get(point, i)) {
//                Set(point, i, -1);
//            }
//        }
//    }

    void PrintTable() {
        std::stringstream ss;
//        ss.width(3);

        for (int x = 1; x <= n; ++x) {
            for (int y = 1; y <= n; ++y) {
                ss << std::setw(3) << Get(x, y) << ' ';
            }
            ss << std::endl;
        }
        std::cout << ss.str();
    }
};

int main() {
//    std ::cout <<"HI"<<std::endl;
    std::ifstream in("test");
    std::cin.rdbuf(in.rdbuf());

    int n, m, q;
    int x, y, s, t, k;
    std::cin >> n >> m >> q;
    std::vector<std::list<int>> pathsFrom(n + 1, std::list<int>());
    std::vector<std::list<int>> pathsTo(n + 1, std::list<int>());

    for (int i = 0; i < m; ++i) {
        std::cin >> x >> y;
        pathsFrom[y].push_back(x);
        pathsTo[x].push_back(y);
    }

    for (int i = 1; i < n + 1; ++i) {
        pathsTo[i].sort();
    }

    SquareTable table(n, pathsFrom, pathsTo);
//
//    std::stringstream answer;
//    for (int x = 0; x <= q; ++x) {
//        std::cin >> s >> t >> k;
//        answer << table.GetKFromPath(s, t, k) << std::endl;
//    }
//    std::cout << answer.str();

    return 0;
}


