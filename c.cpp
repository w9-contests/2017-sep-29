#include <iostream>
#include <list>
#include <vector>
#include <algorithm>

long long nMaxAb2(long long capacity, long long ab2) {
    return (capacity / ab2);
}

long long maxAb2(long long capacity, long long ab2) {
    return (capacity / ab2) * ab2;
}

#define left 0
#define from_left 1
#define from_right 0
#define  right 1

struct max_ride {
    long long distance;
    bool direction_change;

    max_ride() {}

    max_ride(long long capacity, long long ab2, long long additional) {
        distance = maxAb2(capacity, ab2);
        if ((distance + additional) <= capacity) {
            distance += additional;
            direction_change = true;
        } else {
            direction_change = false;
        }
    }
};

void print_ride_info(long long left_to_ride, int last_direction) {
    std::cout << "left_distance: " << left_to_ride << std::endl;
    std::cout << "cur_dir: " << ((from_right == last_direction) ? "from_right" : "from_left") << std::endl;
}

int main() {
    long long b, f, capacity, k;
    std::cin >> b >> capacity >> f >> k;
    long long answer = 0;

    long long total_distance = k * b;
    long long ab = b;
    long long ab2 = ab * 2;
    long long af = f;
    long long af2 = f * 2;
    long long fb = (ab - af);
    long long fb2 = 2 * fb;
    //<editor-fold desc="Trivial case">
    if (capacity > total_distance) {
        std::cout << 0 << std::endl;
        return 0;
    } else if (k <= 2) {
        if (k == 1) {
            // case if capacity > total_distance we have looked
            if ((capacity >= af) && (capacity >= fb)) {
                std::cout << 1 << std::endl;
            } else {
                std::cout << -1 << std::endl;
            }
            return 0;
        } else {
            if ((capacity >= fb2) && (capacity >= af)) {
                std::cout << (((fb2 + af) <= capacity) ? 1 : 2) << std::endl;
            } else {
                std::cout << -1 << std::endl;
            }
            return 0;
        }
    } else if (af2 > capacity || fb2 > capacity) {
        std::cout << -1 << std::endl;
        return 0;
    }
    //</editor-fold>

    long long last_ride;
    int last_direction;
    if (k % 2 == 1) {
        // last move to right
        last_ride = fb + maxAb2(capacity - fb, ab2);
        last_direction = from_left;
        if ((last_ride + af2) <= capacity) {
            last_direction = from_right;
            last_ride += af2;
        }
    } else {
        // last move to left
        last_ride = af + maxAb2(capacity - af, ab2);
        last_direction = from_right;
        if ((last_ride + fb2) <= capacity) {
            last_direction = from_left;
            last_ride += fb2;
        }
    }
    long long left_to_ride = total_distance - last_ride;
    std::vector<max_ride> rides(2);
    rides[from_left] = max_ride(capacity, ab2, af2);
    rides[from_right] = max_ride(capacity, ab2, fb2);

//    std::cout << "from_left: " << rides[from_left].distance << std::endl;
//    std::cout << "from_right: " << rides[from_right].distance << std::endl;

    long long first_ride = af + maxAb2((capacity - af), ab2);
    int first_direction = right;
    if ((first_ride + fb2) <= capacity) {
        first_direction = left;
        first_ride = first_ride + fb2;
    }
    left_to_ride -= first_ride;
    answer = 1;
    while (left_to_ride > 0) {
//        print_ride_info(left_to_ride, last_direction);
        answer += 1;
        left_to_ride -= rides[last_direction].distance;
        last_direction = 1 - last_direction;
    }
    if (first_direction != last_direction){
        ++answer;
    }

//    answer = 0;
//    std::vector<max_ride> rides(2);
//    rides[left] = max_ride(capacity, ab2, af2);
//    rides[right] = max_ride(capacity, ab2, fb2);
//    if (left_to_ride < rides[first_direction].distance) { answer = 1; }
//    else {
//        if ((rides[left].direction_change) && (rides[right].direction_change)) {
//            long long two_way_distance = rides[left].distance + rides[right].distance;
//            long long two_way_quantity = left_to_ride / two_way_distance;
//            answer += 2 * two_way_quantity;
//            left_to_ride -= two_way_distance * two_way_quantity;
//            if (left_to_ride <= 0) {
//                std::cout << answer << std::endl;
//                return 0;
//            }
//        } else if (rides[first_direction].direction_change) {
//            // here we can change a direction only once
//            ++answer;
//            left_to_ride -= rides[first_direction].distance;
//            first_direction = 1 - first_direction;
//            if (left_to_ride <= 0) {
//                std::cout << answer << std::endl;
//                return 0;
//            } else {
//                long long quantity = left_to_ride / rides[first_direction].distance;
//                answer += quantity;
//                left_to_ride -= quantity * rides[first_direction].distance;
//            }
//        } else {
//            // we can not change direction anymore
//            long long quantity = left_to_ride / rides[first_direction].distance;
//            answer += quantity;
//            left_to_ride -= quantity * rides[first_direction].distance;
//        }
//        if (rides[first_direction].distance >= left_to_ride) {
//            answer += 1;
//        } else {
//            answer += 2;
//        }
//    }

    std::cout << answer << std::endl;

    return 0;
}