#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>


bool isCapital(char s) {
    std::cout << std::endl << "===" << s << ' ' << s - 'A' << ' ' << ((s >= 'A') && (s <= 'Z')) << std::endl;
    return ((s >= 'A') && (s <= 'Z'));
}

int main() {
    int n;
    std::string message;
    std::cin >> n >> message;
    int best = 0;
    int cur = 0;
    std::map<char, int> faced;
    for (char s: message) {
        if (!isCapital(s)) {
            std::cout << s << " is not capital";
            if (cur > best) {
                best = cur;
                cur = 0;
                std::cout << faced.size() << ' ';
                faced.clear();
                std::cout << faced.size() << std::endl;

            } else {
                if (faced.find(s) == faced.end()) {
                    std::cout << s << ' ';
                    faced[s] = 1;
                    cur += 1;
                }
            }
        }
    }
    if (best <= 1) {
        std::cout << 0 << std::endl;
    } else {
        std::cout << best << std::endl;
    }

    return 0;
}