#include <iostream>
#include <fstream>
#include "header.h"

int main() {
    std::ifstream in("test");
    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
    std::cin.rdbuf(in.rdbuf());
    f();
    return 0;
}