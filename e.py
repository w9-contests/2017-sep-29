import sys

sys.stdin = open('test', 'r')


def print_array(arr):
    print '%i ' * len(arr) % tuple(arr)


def print_selection_table(selection_table):
    for i in range(len(selection_table)):
        print selection_table[i]


SAVE_TIME = 0
RESISTANCE = 1
COST = 2
ORIGINAL_INDEX = 3

n = input()
goods = []
max_resistance = 0
min_save_time = 20
for i in range(n):
    next_good = tuple(int(x) for x in raw_input().split() + [i + 1])
    if next_good[SAVE_TIME] < next_good[RESISTANCE]:
        max_resistance = max_resistance if max_resistance >= next_good[RESISTANCE] else next_good[RESISTANCE]
        min_save_time = min_save_time if min_save_time <= next_good[SAVE_TIME] else next_good[SAVE_TIME]
        goods.append(next_good)

goods_count = len(goods)
if 0 == goods_count:
    print 0
    print 0
else:
    goods.sort(key=lambda item: item[1])
    # format of cell in table (value==cost_of_saved_goods, saved_good_index, previously_saved_index)
    selection_table = [[(0, -1, -1, -1) for _ in range(max_resistance)] for __ in range(goods_count)]
    # selection_table = [[(0, -1, -1, -1)] * max_resistance] * goods_count
    selection_table[0] = [(0, -1, -1, -1)] * goods[0][SAVE_TIME] + \
                         [(goods[0][COST], 0, -1, -1)] * (max_resistance - goods[0][SAVE_TIME])
    # print selection_table[0]
    # print_selection_table(selection_table)
    # print_selection_table(goods)
    for i in range(1, goods_count):
        next_good = goods[i]
        save_time = next_good[SAVE_TIME]
        resistance = next_good[RESISTANCE]
        for j in range(min_save_time, resistance):
            prev_res = selection_table[i - 1][j]
            if save_time <= j:
                if next_good[COST] + selection_table[i - 1][j - save_time][0] > prev_res[0]:
                    selection_table[i][j] = (next_good[COST] + selection_table[i - 1][j - save_time][0],
                                             i, i - 1, j - save_time)
                    # print selection_table[i][j]
                else:
                    selection_table[i][j] = (prev_res[0], -1, i - 1, j)
            else:
                selection_table[i][j] = (prev_res[0], -1, i - 1, j)
        for j in range(resistance, max_resistance):
            selection_table[i][j] = selection_table[i][resistance - 1]
        # print selection_table[i]
    # print_selection_table(selection_table)
    current = selection_table[goods_count - 1][max_resistance - 1]
    print current[0]
    saved_goods = []
    while True:
        if current[1] != -1:
            saved_goods.append(goods[current[1]][ORIGINAL_INDEX])
        if (-1 != current[2]) and (-1 != current[3]):
            current = selection_table[current[2]][current[3]]
        else:
            break
    saved_goods.reverse()
    print len(saved_goods)
    print_array(saved_goods)
